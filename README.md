# IPtvDream 4X

Плагин для просмотра iptv на ресивирах с enigma2.

Отчёты об ошибках, пожелания и особенно пулл-реквесты приветствуются!

# Release notes

## 4.6

- ввод букв в логин-пароль по синей кнопке
- исправлен крэш едема при отсутствии интернета
- добавлена радуга и телепром
- выход из плагина по кнопке экзит

## 4.5

- Поддержка избранных каналов
- Возможность выбора двух типов управления (enigma и neutrino)
- Улучшение интерфейса
- Исправление багов

## 4.3

- Первая версия 4X